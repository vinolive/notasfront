import { ItemService } from './../service/item.service';
import { Component, OnInit } from '@angular/core';
import { TopicoService } from '../service/topico.service';

@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.scss']
})
export class BaseComponent implements OnInit {
  novoTopico : string = "";
  pegaId : any = [];
  topico : any [] = [];
  items : any[] = [];
  conjunto : any = [];

  constructor(private service:TopicoService,private serviceItem: ItemService) { }

  ngOnInit(): void {
    this.pegaTopicos();
    this.pegaItems();
  }

  public pegaTopicos(){
    this.service.getAllTopicos().subscribe((topicosPegos:any[])=>{
      this.topico = topicosPegos;
    })
  }
  public pegaItems(){
    this.serviceItem.getAllItems().subscribe((itemsPegos:any[])=>{
      this.items = itemsPegos;
      this.geraConjunto();
    })
  }

  public geraConjunto(){
    for(let i = 0;i<this.topico.length;i++){
      let adicao = {
        idTopico:this.topico[i].idTopico,
        topico:this.topico[i].nome,
        items:[{}]
      }
      for(let j = 0;j<this.items.length;j++){
        if(this.items[j].idTopico==i+1){
          adicao.items.push(this.items[j])
        }
      }
      adicao.items.shift();
      this.conjunto.push(adicao);
    }

    console.log(this.conjunto);

  }

  public criaTopico(){
    let topico = {
      nome : this.novoTopico
    }
    this.service.postTopico(topico).subscribe((resultado)=>{
      console.log(resultado);
    },error=>{
      console.error(error);
    })
    this.novoTopico="";
    window.location.reload();
  }

  public deletaTopico(a:any[]){
    this.pegaId=a;
    let id=this.pegaId.idTopico
    this.service.deletarTopico(id).subscribe((res)=>{
        console.log(res);
    },error=>{
        console.error(error);
    })
    window.location.reload();
    }

}
