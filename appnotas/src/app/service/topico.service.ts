import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TopicoService {
  private url : string ="http://localhost:8080/topicos"
  constructor(private httpClient: HttpClient) {}

  public getAllTopicos():Observable<any[]>{
    return this.httpClient.get<any[]>(this.url);
  }

  public postTopico( topico: any ): Observable<any>{
    return this.httpClient.post<any>(this.url,topico)
}

  public deletarTopico(id:any):Observable<any>{
    let endPoint = "/"+id
    return this.httpClient.delete<any>(this.url+endPoint,id)
}
}
