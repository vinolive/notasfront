import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ItemService {
  private url : string ="http://localhost:8080/item"
  constructor(private httpClient: HttpClient) {}

  public getAllItems():Observable<any[]>{
    return this.httpClient.get<any[]>(this.url);
  }
}
